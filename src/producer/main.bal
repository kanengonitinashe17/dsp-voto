
import ballerinax/kafka;
import ballerina/graphql;
import ballerina/io;
import ballerina/docker;
import ballerina/kubernetes;
 

kafka:ProducerConfiguration voting_system = {
	bootstrapServers: "localhost:9092",
	clientId: "register-candidate",
	acks: "all",
	retryCount: 3
//	valueSerializerType: kafka:SER_STRING,
//	keySerializerType: kafka:SER_INT
};


map<Candidate> registered_candidate_voters ={};
map<Registered_voter> register_vote ={};
map<Vote> accepted_vote ={};
map<Regected_vote> rejected_vote ={};



kafka:Producer voting_systems =checkpanic new (voting_system);
@kubernetes:Deployment {
    image:"voting_system",
    name:"voting-producer"
}
@docker:Config {
	name: "vsystem",
	tag: "v1.0"
}


service graphql:Service /graphql on new graphql:Listener(9090) {
 //register candidate
    resource function get cregistration(string name,int id,string ruling_party) returns string {
           Candidate candidates ={name,id,ruling_party};
            registered_candidate_voters[id.toString()] = {name:name,id:id,ruling_party:ruling_party};
          
        return "Candidate registered succesfully : " + name;
    }
    //vote 
     resource function get vote(int voterID,int candidateID) returns string {
            Vote vote_info ={voterID,candidateID};
        //    //check if the details are correct
       
            if (register_vote.hasKey(voterID.toString()) && registered_candidate_voters.hasKey(candidateID.toString()) ){
                 io:println(registered_candidate_voters);
                 accepted_vote[voterID.toString()] ={voterID:voterID,candidateID:candidateID};
                 
                string? candidate_name = registered_candidate_voters[candidateID.toString()]["name"];
              
               byte[] serialisedMsg = candidate_name.toString().toBytes();

              checkpanic voting_systems->sendProducerRecord({
                                    topic: "projectedresults",
                                    value: serialisedMsg });

             checkpanic voting_systems->flushRecords();

            }else{
                string rejected_votes = "wrong voterID";
                rejected_vote[voterID.toString()] = {voterID:voterID,candidateID:candidateID,rejected_vote:rejected_votes};
               
            }
      
        return "voted succefully " ;
    }

// register as a voter
    // register as a voter
     resource function get register_vote(string name,int namibian_id) returns string {
            Registered_voter vote_info ={name,namibian_id};
            // Candidate candidate ={name,id,ruling_party};
            register_vote[namibian_id.toString()] = {name:name,namibian_id:namibian_id};

        
            io:println(register_vote);
        return "voter registered succesfully, " + name;
    }

    //send  registered  candidates
     resource function get send_registered_candidates(int id) returns string {
         
          
              byte[] serialisedMsg = registered_candidate_voters.toString().toBytes();
                 checkpanic voting_systems->sendProducerRecord({
                                    topic: "votinginfo",
                                    value: serialisedMsg });

             checkpanic voting_systems->flushRecords();

            io:println(register_vote);
        return "candidates sent " ;
    }
    //SEND registered  voters 
   resource function get send_registered_voters(int id) returns string {
         
          
              byte[] serialisedMsg = register_vote.toString().toBytes();
                 checkpanic voting_systems->sendProducerRecord({
                                    topic: "disputed",
                                    value: serialisedMsg });

             checkpanic voting_systems->flushRecords();

            io:println(register_vote);
        return "voter roll printed " ;
    }
//send votes
resource function get send_votes(int id) returns string {
         
          
              byte[] serialisedMsg = accepted_vote.toString().toBytes();
                 checkpanic voting_systems->sendProducerRecord({
                                    topic: "archive",
                                    value: serialisedMsg });

             checkpanic voting_systems->flushRecords();

            io:println(register_vote);
        return "votes archived " ;
    }
}
//records
public type Candidate record {
    string name;
    int id;
    string ruling_party;
};
public type Vote record {
    int voterID;
    int candidateID;
    
};
public type Registered_voter record {
    string name;
    int namibian_id;
};
public type Regected_vote record {
    int voterID;
    int candidateID;
    string rejected_vote;
};
