
import ballerinax/kafka;
//import ballerina/lang.'string;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/docker;


kafka:ConsumerConfiguration consConf = {
    bootstrapServers: "localhost:9092",
    groupId: "group-id",

    topics: ["disputed"],
    pollingIntervalInMillis: 1000, 
    //keyDeserializerType: kafka:DES_INT,
    //valueDeserializerType: kafka:DES_STRING,
    autoCommit: false
};


listener kafka:Listener cons = new (consConf);

@kubernetes:Deployment {
    image:"disputed ",
    name:"disputed  "
}
@docker:Config {
	name: "Disputed",
	tag: "v1.0"
}

map<Candidate> candidates_voted_for ={};

service kafka:Service on cons {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) {
        foreach var kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }

        var commitResult = caller->commit();

        if (commitResult is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", err = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
    byte[] value = kafkaRecord.value;
    string|error messageContent = string:fromBytes(value);
    
    if (messageContent is string) {
        io:println("********DISPUTED VOTES********");
        io:println("Value: " + messageContent);
    } else {
        log:printError("Invalid value type received");
    }
}
public type Candidate record {
    string name;
    int id;
    string ruling_party;
};