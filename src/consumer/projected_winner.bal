
import ballerinax/kafka;
//import ballerina/lang.'string;
import ballerina/log;
import ballerina/io;
import ballerina/kubernetes;
import ballerina/docker;
kafka:ConsumerConfiguration consConf = {
    bootstrapServers: "localhost:9092",
    groupId: "group-id",

    topics: ["projectedresults"],
    pollingIntervalInMillis: 1000, 
    //keyDeserializerType: kafka:DES_INT,
    //valueDeserializerType: kafka:DES_STRING,
    autoCommit: false
};


listener kafka:Listener cons = new (consConf);
@kubernetes:Deployment {
    image:"projectedwinner",
    name:"kafka-consumer"
}
@docker:Config {
	name: "projectedwinner",
	tag: "v1.0"
}

map<Candidate> candidates_voted_for ={};

service kafka:Service on cons {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) {
        foreach var kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }

        var commitResult = caller->commit();

        if (commitResult is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", err = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
    byte[] value = kafkaRecord.value;
    string|error candidateName = string:fromBytes(value);
    
    if (candidateName is string) {

        if (candidates_voted_for.hasKey(candidateName)){

               int increment_vote = <int>candidates_voted_for[candidateName]["number_of_votes"]+1;

               candidates_voted_for[candidateName]["votes"]=increment_vote;
               
        }else{
              candidates_voted_for[candidateName]["votes"]={name:candidateName,number_of_votes:1};
        } 
        io:println("************PROJECTED WINNER************");
         io:println( candidates_voted_for);
    } else {
        log:printError("Invalid value type received");
    }
}
public type Candidate record {
    string name;
   
    int number_of_votes;
};